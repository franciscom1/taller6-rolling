export const obtenerData = async () => {
    try {
        const resp = await fetch('https://api.coincap.io/v2/assets?limit=10')
        const {data} = await resp.json()
        return data
    } catch (error) {
        console.log(error)
    }
}

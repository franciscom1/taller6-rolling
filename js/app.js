import {obtenerData} from '../helpers/fetch.js'
const btn = document.querySelector('#btn-update')
let criptos = []


const aa = () => {
    const tBody = document.querySelector('#tableBody')
    tBody.innerHTML = ''
    obtenerData().then(resp => {
        criptos = [...resp]
        crearTabla()
    })
}


const crearTabla = () => {
    const tBody = document.querySelector('#tableBody')
    criptos.forEach(cripto => {
        const {rank, name, priceUsd, marketCapUsd, vwap24Hr, supply, volumeUsd24Hr, changePercent24Hr} = cripto
        const row = document.createElement('tr')
        row.innerHTML = `
        <td>${rank}</td>
        <td>${name}</td>
        <td>${numeral(priceUsd).format('$0,0.00')}</td>
        <td>${numeral(marketCapUsd).format('($ 0.00 a)')}</td>
        <td>${numeral(vwap24Hr).format('$0,0.00')}</td>
        <td>${numeral(supply).format('($ 0.00 a)')}</td>
        <td>${numeral(volumeUsd24Hr).format('($ 0.00 a)')}</td>
        <td>${parseFloat(changePercent24Hr).toFixed(2)} %</td>`
        
        tBody.append(row)
        
    });
}

aa()

btn.addEventListener("click", aa)